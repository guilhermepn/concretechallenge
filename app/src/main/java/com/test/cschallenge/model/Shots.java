package com.test.cschallenge.model;

import java.io.Serializable;

public class Shots implements Serializable{
    private int     id;
    private String  title;
    private String  description;
    private int     height;
    private int     width;
    private int     likes_count;
    private int     comments_count;
    private int     rebounds_count;
    private String  url;
    private String  short_url;
    private int     views_count;
    private int     rebound_source_id;
    private String  image_url;
    private String  image_teaser_url;
    private String  image_400_url;
    private Player  player;
    private String  created_at;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public int getLikes_count() {
        return likes_count;
    }

    public int getComments_count() {
        return comments_count;
    }

    public int getRebounds_count() {
        return rebounds_count;
    }

    public String getUrl() {
        return url;
    }

    public String getShort_url() {
        return short_url;
    }

    public int getViews_count() {
        return views_count;
    }

    public int getRebound_source_id() {
        return rebound_source_id;
    }

    public String getImage_url() {
        return image_url;
    }

    public String getImage_teaser_url() {
        return image_teaser_url;
    }

    public String getImage_400_url() {
        return image_400_url;
    }

    public Player getPlayer() {
        return player;
    }

    public String getCreated_at() {
        return created_at;
    }
}
