package com.test.cschallenge.model;

import android.telecom.Call;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

public interface DribbbleApiInterface {
    @GET("/shots/popular")
    void getPage(@Query("page") int npage, Callback<Page> page);
}
