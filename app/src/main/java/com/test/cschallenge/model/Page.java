package com.test.cschallenge.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by guilherme.nery on 18/08/2015.
 */
public class Page implements Serializable{
    private int page;
    private int per_page;
    private int pages;
    private int total;
    private List<Shots> shots;

    public int getPage() {
        return page;
    }

    public int getPer_page() {
        return per_page;
    }

    public int getPages() {
        return pages;
    }

    public int getTotal() {
        return total;
    }

    public List<Shots> getShots() {
        return shots;
    }
}
