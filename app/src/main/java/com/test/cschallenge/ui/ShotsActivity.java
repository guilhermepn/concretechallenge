package com.test.cschallenge.ui;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.os.Bundle;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.ProgressBar;

import com.test.cschallenge.R;
import com.test.cschallenge.adapters.RecyclerAdapter;
import com.test.cschallenge.model.DribbbleApiInterface;
import com.test.cschallenge.model.Page;
import com.test.cschallenge.model.Shots;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ShotsActivity extends AppCompatActivity{
    private static final                String API_URL = "http://api.dribbble.com";
    private RecyclerView                mRecyclerView;
    private StaggeredGridLayoutManager  gridLayoutManager;
    private RecyclerAdapter             mAdapter;
    private ArrayList<Shots>            mDataset;
    private ProgressBar                 mProgressBar;
    private int                         previousTotal = 0;
    private boolean                     loading = true;
    private int                         totalItemCount;
    private int                         page_num = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app);

        FragmentManager fragmentManager = getSupportFragmentManager();
        mDataset          = new ArrayList<Shots>();
        mAdapter          = new RecyclerAdapter(getBaseContext(), mDataset, fragmentManager);
        gridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        mProgressBar      = (ProgressBar)  findViewById(R.id.main_progressbar);
        mRecyclerView     = (RecyclerView) findViewById(R.id.shots_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

       /**
        * Consome a API pelo Retrofit
        */
        retrieveShots();

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener(){
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy){
                super.onScrolled(recyclerView, dx, dy);
                if (dx == 0 && dy == 0) return;
                totalItemCount = gridLayoutManager.getItemCount();
                if(loading){
                    if(totalItemCount > previousTotal){
                        loading       = false;
                        previousTotal = totalItemCount;
                    }
                }

                int[]   into         = gridLayoutManager.findFirstCompletelyVisibleItemPositions(null);
                int     lastPosition = recyclerView.getChildCount() - 1;
                Arrays.sort(into);
                if((into != null) && (lastPosition == into[into.length - 1])){
                    retrieveShots();
                    loading = true;
                }
            }
        });
    }

    /**
     * Callback do Retrofit
     */
    private Callback callback = new Callback(){
        @Override
        public void success(Object o, Response response){
            mDataset.addAll(((Page)o).getShots());
            mAdapter.setDataset(mDataset);
            mAdapter.notifyDataSetChanged();
            mProgressBar.setVisibility(View.GONE);
        }

        @Override
        public void failure(RetrofitError retrofitError){
        }
    };

    private void retrieveShots(){
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(API_URL)
                .build();
        DribbbleApiInterface methods = restAdapter.create(DribbbleApiInterface.class);
        methods.getPage(page_num++, callback);
    }
}