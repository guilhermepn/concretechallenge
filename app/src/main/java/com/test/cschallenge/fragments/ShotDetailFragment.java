package com.test.cschallenge.fragments;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.test.cschallenge.R;
import com.test.cschallenge.model.Player;
import com.test.cschallenge.model.Shots;

public class ShotDetailFragment extends Fragment{

    private Shots       mShot;
    private Context     mContext;
    private ImageView   shotBigImageview;
    private TextView    shotDetailTitleTextview;
    private ImageView   authorAvatarImageview;
    private TextView    authorUsernameTextview;
    private TextView    authorNameTextview;
    private TextView    authorLikesReceivedTextview;
    private TextView    authorCommentsReceivedTextview;
    private TextView    authorFollowersTextview;
    private TextView    authorUrlTextview;
    private Button      detailBackButton;

    @Override
    public void setArguments(Bundle args){
        mShot = (Shots) args.getSerializable("shot");
    }

    public ShotDetailFragment(){
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.detail_layout, container, false);
        try{
            mContext = getActivity();
            Player player   = mShot.getPlayer();
            Shots shot      = mShot;

            shotBigImageview                = (ImageView)   v.findViewById(R.id.shot_detail_image_view);
            shotDetailTitleTextview         = (TextView)    v.findViewById(R.id.shot_detail_title_textview);
            authorAvatarImageview           = (ImageView)   v.findViewById(R.id.author_avatar_imageview);
            authorUsernameTextview          = (TextView)    v.findViewById(R.id.author_username_textview);
            authorLikesReceivedTextview     = (TextView)    v.findViewById(R.id.author_likes_received_textview);
            authorNameTextview              = (TextView)    v.findViewById(R.id.author_name_textview);
            authorFollowersTextview         = (TextView)    v.findViewById(R.id.author_detail_followers_textview);
            authorCommentsReceivedTextview  = (TextView)    v.findViewById(R.id.author_detail_comments_received_textview);
            authorUrlTextview               = (TextView)    v.findViewById(R.id.author_url_textview);
            detailBackButton                = (Button)      v.findViewById(R.id.detail_back_btn);

            Picasso.with(mContext).load(shot.getImage_url())
                    .fit()
                    .centerCrop()
                    .into(shotBigImageview);
            shotDetailTitleTextview.setText(shot.getTitle());
            Picasso.with(mContext).load(shot.getPlayer().getAvatar_url()).resize(200, 200).into(authorAvatarImageview);
            authorUsernameTextview.setText(authorUsernameTextview.getText() + " " + player.getUsername());
            authorNameTextview.setText(player.getName());
            authorLikesReceivedTextview.setText(String.valueOf(player.getLikes_received_count()));
            authorCommentsReceivedTextview.setText(String.valueOf(player.getComments_received_count()));
            authorFollowersTextview.setText(String.valueOf(player.getFollowers_count()));
            authorUrlTextview.setText(player.getWebsite_url());

            View.OnClickListener clickListener = new View.OnClickListener(){
                @Override
                public void onClick(View view){
                    selfClose();
                }
            };
            detailBackButton.setOnClickListener(clickListener);
        }catch(NullPointerException npe){
            Log.d("CsChallenge", "Error: " + npe.getMessage());
        }
        return v;
    }

    private void selfClose(){
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.remove(this);
        fragmentTransaction.commit();
    }
}
