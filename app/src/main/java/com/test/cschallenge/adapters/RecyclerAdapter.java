package com.test.cschallenge.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.test.cschallenge.R;
import com.test.cschallenge.fragments.ShotDetailFragment;
import com.test.cschallenge.model.Player;
import com.test.cschallenge.model.Shots;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder>{
    private ArrayList<Shots>    shots;
    private Context             context;
    private FragmentManager     mFragmentManager;

    public RecyclerAdapter(Context context, ArrayList<Shots> dataset, FragmentManager fragmentManager){
        this.context = context;
        this.shots   = dataset;
        this.mFragmentManager = fragmentManager;
    }

    public void setDataset(ArrayList<Shots> dataset){
        this.shots = dataset;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public ImageView    shotImageview;
        public TextView     shotTitleTextview;
        public TextView     shotAuthorTextview;
        public TextView     shotLikesTextview;
        public TextView     shotViewsTextview;
        public ImageView    authorAvatarImageview;
        public TextView     authorUsernameTextview;
        public TextView     authorNameTextview;
        public TextView     authorLikesReceivedTextview;

        public ViewHolder(View v){
            super(v);
            this.shotImageview              = (ImageView)    v.findViewById(R.id.shot_image_view);
            this.shotTitleTextview          = (TextView)     v.findViewById(R.id.shot_title_textview);
            this.shotLikesTextview          = (TextView)     v.findViewById(R.id.shot_detail_likes_textview);
            this.shotViewsTextview          = (TextView)     v.findViewById(R.id.shot_detail_views_textview);
            this.authorAvatarImageview      = (ImageView)    v.findViewById(R.id.author_avatar_imageview);
            this.authorUsernameTextview     = (TextView)     v.findViewById(R.id.author_username_textview);
            this.authorLikesReceivedTextview= (TextView)     v.findViewById(R.id.author_likes_received_textview);
            this.authorNameTextview         = (TextView)     v.findViewById(R.id.author_name_textview);
            this.shotAuthorTextview         = (TextView)     v.findViewById(R.id.shot_author_textview);
        }
    }

    /**
     * Criando e inflando views
     */
    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.shot_layout, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    /**
     * Populando o holder
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position){
        Player player = shots.get(position).getPlayer();
        final Shots shot = shots.get(position);

        Picasso.with(context).load(shot.getImage_url())
                .fit()
                .centerCrop()
                .into(holder.shotImageview);
        holder.shotLikesTextview.setText(String.valueOf(shot.getLikes_count()) + " ");
        holder.shotViewsTextview.setText(String.valueOf(shot.getViews_count()));
        holder.shotTitleTextview.setText(shot.getTitle());
        holder.shotAuthorTextview.setText(player.getName());

        View.OnClickListener clickListener = new View.OnClickListener(){
            @Override
            public void onClick(View view){
                ShotDetailFragment shotDetailFragment = new ShotDetailFragment();
                Bundle args = new Bundle();
                args.putSerializable("shot", shot);
                shotDetailFragment.setArguments(args);
                FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_detail_framelayout, shotDetailFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        };
        holder.shotImageview.setOnClickListener(clickListener);
    }

    @Override
    public int getItemCount(){
        return (shots == null) ? 0 : shots.size();
    }
}